import Chart from 'chart.js';
import { initGlobalOptions } from "@/components/gbedu-core/Charts/config";
export default {
  mounted() {
    initGlobalOptions();
  }
}
