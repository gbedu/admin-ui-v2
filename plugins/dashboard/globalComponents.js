import Badge from '~/components/gbedu-core/Badge.vue';
import BaseAlert from "~/components/gbedu-core/BaseAlert";
import BaseButton from '~/components/gbedu-core/BaseButton.vue';
import BaseCheckbox from '~/components/gbedu-core/Inputs/BaseCheckbox.vue';
import BaseDropdown from '~/components/gbedu-core/BaseDropdown.vue';
import BaseHeader from '~/components/gbedu-core/BaseHeader';
import BaseInput from '~/components/gbedu-core/Inputs/BaseInput.vue';
import BaseNav from "~/components/gbedu-core/Navbar/BaseNav";
import BasePagination from "~/components/gbedu-core/BasePagination";
import BaseProgress from "~/components/gbedu-core/BaseProgress";
import BaseRadio from "~/components/gbedu-core/Inputs/BaseRadio";
import BaseSwitch from '~/components/gbedu-core/BaseSwitch.vue';
import Card from '~/components/gbedu-core/Cards/Card.vue';
import Modal from '~/components/gbedu-core/Modal.vue';
import RouteBreadcrumb from '~/components/gbedu-core/Breadcrumb/RouteBreadcrumb';
import StatsCard from '~/components/gbedu-core/Cards/StatsCard.vue';
import { Input, Tooltip, Popover } from 'element-ui';
import { ValidationProvider, ValidationObserver } from 'vee-validate';
import "vue-multiselect/dist/vue-multiselect.min.css";
import Multiselect from "vue-multiselect";
import flatPicker from "vue-flatpickr-component";
import "flatpickr/dist/flatpickr.css";
import Vue from 'vue';

/**
 * You can register global components here and use them as a plugin in your main Vue instance
 */

Vue.component(Badge.name, Badge);
Vue.component(BaseAlert.name, BaseAlert);
Vue.component(BaseButton.name, BaseButton);
Vue.component(BaseCheckbox.name, BaseCheckbox);
Vue.component(BaseHeader.name, BaseHeader);
Vue.component(BaseInput.name, BaseInput);
Vue.component(BaseDropdown.name, BaseDropdown);
Vue.component(BaseNav.name, BaseNav);
Vue.component(BasePagination.name, BasePagination);
Vue.component(BaseProgress.name, BaseProgress);
Vue.component(BaseRadio.name, BaseRadio);
Vue.component(BaseSwitch.name, BaseSwitch);
Vue.component(Card.name, Card);
Vue.component(Modal.name, Modal);
Vue.component(StatsCard.name, StatsCard);
Vue.component(RouteBreadcrumb.name, RouteBreadcrumb);
Vue.component(Input.name, Input);
Vue.component('validation-provider', ValidationProvider)
Vue.component('validation-observer', ValidationObserver)
Vue.use(Tooltip);
Vue.use(Popover);
Vue.component("MultiSelect", Multiselect);
Vue.component("FlatPicker", flatPicker);
