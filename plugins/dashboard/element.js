import Vue from 'vue';
import { Loading, Select, Option, Button, OptionGroup, Icon } from 'element-ui';

Vue.use(Icon);
Vue.use(Select);
Vue.use(Option);
Vue.use(Button);
Vue.use(OptionGroup);
Vue.use(Loading.directive);

Vue.prototype.$loading = Loading.service;