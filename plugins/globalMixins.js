import Vue from "vue";

Vue.mixin({
  methods: {
    //SELECT AN ELEMENT WITH ID
    selectDocument(payload) {
      document.getElementById(payload).click();
    },
    filterArtistTracks(arr) {
      return arr.map(({ node }) => {
        const container = {};
        container.id = node.track.id || "";
        container.title = node.track.title || "";
        container.duration = node.track.duration || 0;
        container.track_image = node.track.track_image
          ? node.track.track_image.url
          : "";
        container.artists = node.track.artists || "";
        container.features = node.track.features || [];
        container.features1 = container.features;
        container.genres = node.track.genres || [];
        container.genres1 = container.genres;
        if (node.track.albums && node.track.albums.length > 0) {
          container.track_album =
            node.track.albums && node.track.albums.length > 0
              ? node.track.albums[0].album
              : {};
          container.album_track =
            node.track.albums && node.track.albums.length > 1
              ? node.track.albums[1].album
              : node.track.albums &&
                node.track.albums[0].album &&
                node.track.albums[0].album.type == "release"
              ? node.track.albums[0].album
              : {};
          container.songProp =
            container.track_album &&
            container.track_album.id &&
            node.track.albums &&
            node.track.albums.length > 1 &&
            node.track.albums[1].album
              ? {
                  id: "SINGLE_AND_ALBUM_TRACK",
                  title: "SINGLE_AND_ALBUM_TRACK"
                }
              : container.track_album &&
                container.track_album.type &&
                container.track_album.type == "release"
              ? { id: "ALBUM_TRACK", title: "ALBUM_TRACK" }
              : { id: "SINGLE_TRACK", title: "SINGLE_TRACK" };
          container.songProp1 = container.songProp;
          container.myAlbum =
            node.track.albums &&
            node.track.albums.length == 1 &&
            node.track.albums[0].album.type == "release"
              ? [node.track.albums[0].album]
              : [];
          container.produced_by =
            node.track.produced_by == null ? [] : node.track.produced_by;
          container.produced_by1 = container.produced_by;
          container.written_by =
            node.track.written_by == null ? [] : node.track.written_by;
          container.written_by1 = container.written_by;
          container.collaborators =
            node.track.artists == null ? [] : node.track.artists;
          container.collaborators1 = container.collaborators;
          container.release_date = node.track.release_date;
          container.sort_title = node.track.sort_title;
          container.track_position = node.track.albums[0].position;
          container.audio = node.track.audio ? node.track.audio.url : "";
        }
        return container;
      });
    },
    jsUcfirst(string) {
      let stringArr = string.split(" ");
      if (stringArr.length > 0) {
        for (var i = 0; i < stringArr.length; i++) {
          stringArr[i] =
            stringArr[i].charAt(0).toUpperCase() +
              stringArr[i].slice(1).toLowerCase() || stringArr[i];
          if (i == stringArr.length - 1) {
            return stringArr.join(" ");
          }
        }
      } else {
        return (
          string.charAt(0).toUpperCase() + string.slice(1).toLowerCase() ||
          string
        );
      }
    },
    filterSearchArtist(arr) {
      return arr.map(({ node }) => {
        const container = {};
        container.id = node.item.id || "";
        container.name = node.item.name || "";
        return container;
      });
    }
  }
});
