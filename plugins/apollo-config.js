import {
  InMemoryCache,
  IntrospectionFragmentMatcher
} from "apollo-cache-inmemory";
const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData: {
    __schema: {
      types: []
    }
  }
});

export default ({ env }) => {
  return {
    httpEndpoint: env.BASE_URL,
    cache: new InMemoryCache({ fragmentMatcher })
  };
};
