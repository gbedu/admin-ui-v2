export default async function ({ store, redirect }) {
  const { authUser } = store.state;
  if (!authUser) {
    return redirect("/login");
  }
  return redirect("/home/content")
}
