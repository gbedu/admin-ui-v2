export default function({ store, redirect }) {
  const { authUser } = store.state;
  if (authUser) {
    return redirect('/');
  }
}
