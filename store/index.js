/* eslint-disable no-empty-pattern */
import Vue from "vue";
import Vuex from "vuex";
import getGenres from "~/gql/genres.gql";
import getArtists from "~/gql/artists.gql";
import createGenresGql from "~/gql/createGenre.gql";
import createArtist from "~/gql/createArtist.gql";

import currentAdmin from "~/gql/currentAdmin.gql";

Vue.use(Vuex);

export const state = () => ({
  pageLoaded: false, //If page is loaded is true it checks and verifies token
  authUser: null, //Holds the users data
  token: null, //Holds the users session token
  modalLoading: false, //Holds the users session token
  artists: [],
  genres: [],
  albums: [],
  loadingData: false
});

export const getters = {
  getAuthUser: state => state.authUser, //Gets the user data
  getToken: state => state.token, //Gets the users authentication token
  getModalLoading: state => state.modalLoading //Gets the users authentication token
};

export const mutations = {
  //Stores authentication token in cookies and vuex(user session)
  create_cookie(state, { value, expires }) {
    let expiration = new Date(expires * 1000).toUTCString();
    let cookie =
      encodeURI("g-p-a-token=") +
      encodeURI(value) +
      ";expires=" +
      expiration +
      "; path=/;";
    document.cookie = cookie;
    state.token = value;
  },
  set_user: function(state, user) {
    state.authUser = user;
  },
  set_add_genre: function(state, genre) {
    state.genres.push(genre);
  },
  set_add_artist: function(state, genre) {
    state.artists.push(genre);
  },
  set_page_state: function(state, payload) {
    state.pageLoaded = payload;
  },
  set_genres: function(state, genres) {
    state.genres = genres;
  },
  set_artists: function(state, artists) {
    state.artists = artists;
  },
  set_albums: function(state, albums) {
    state.albums = albums;
  },
  set_modalLoading: function(state, status) {
    state.modalLoading = status;
  },
  filterGenres: function(state, arr) {
    state.pageLoaded = false;
    return arr.map(({ node }) => {
      const container = {};
      container.id = node.id || "";
      container.name = node.name || "";

      return container;
    });
  }
};

export const actions = {
  async nuxtServerInit({ commit, dispatch }, { app }) {
    await dispatch("fetchGenres");
    const clientApollo = app.apolloProvider.defaultClient;
    try {
      const res = await clientApollo.query({
        query: currentAdmin
      });

      commit("set_user", res.data.adminUser);
    } catch (err) {
      console.log("unauthenticated user", err);
      commit("set_user", null);
    }
  },
  //Handles login for storing token
  login({ commit }, { user, access_token }) {
    return new Promise(resolve => {
      commit("create_cookie", access_token);
      commit("set_user", user);
      resolve(true);
    });
  },
  logout() {
    var cookies = document.cookie.split("; ");
    window.localStorage.removeItem("g-p-a-app");
    for (var c = 0; c < cookies.length; c++) {
      var d = window.location.hostname.split(".");
      while (d.length > 0) {
        var cookieBase =
          encodeURIComponent(cookies[c].split(";")[0].split("=")[0]) +
          "=; expires=Thu, 01-Jan-1970 00:00:01 GMT; domain=" +
          d.join(".") +
          " ;path=";
        var p = location.pathname.split("/");
        document.cookie = cookieBase + "/";
        while (p.length > 0) {
          document.cookie = cookieBase + p.join("/");
          p.pop();
        }
        d.shift();
      }
    }
  },
  async addGenre({ commit }, payload) {
    return new Promise((resolve, reject) => {
      try {
        const clientApollo = this.app.apolloProvider.defaultClient;
        clientApollo
          .mutate({
            mutation: createGenresGql,
            variables: payload
          })
          .then(({ data }) => {
            const addedGenre = {
              name: data.createGenre.name,
              id: data.createGenre.id
            };
            commit("set_add_genre", addedGenre);
            resolve(addedGenre);
          });
      } catch (e) {
        this.submitting = false;
        console.log(e);

        this.$notify.error({
          message: "Error occurred while processing"
        });
        reject(e);
      }
    });
  },
  async addArtist({ commit }, payload) {
    return new Promise((resolve, reject) => {
      try {
        const clientApollo = this.app.apolloProvider.defaultClient;
        clientApollo
          .mutate({
            mutation: createArtist,
            variables: payload
          })
          .then(({ data }) => {
            const addedArtist = {
              name: data.createArtist.name,
              id: data.createArtist.id
            };
            commit("set_add_artist", addedArtist);
            resolve(addedArtist);
          });
      } catch (e) {
        this.submitting = false;
        this.$notify.error({
          message: "Error occurred while processing"
        });
        reject(e);
      }
    });
  },
  //Handles genre pulling and storing
  // async loadGenres({ commit }) {
  //   return new Promise(resolve => {
  //     this.$axios
  //       .post("/list_genres")
  //       .then(resp => {
  //         commit("set_genre", resp.data.data.genres);
  //         resolve({ genres: resp.data.data.genres });
  //       })
  //       .catch(err => console.log(err));
  //   });
  // },
  async fetchGenres({ commit }) {
    const clientApollo = this.app.apolloProvider.defaultClient;
    const response = await clientApollo.query({
      query: getGenres,
      variables: { first: 2000, after: "0" }
    });
    let genres = response.data.genres.edges.map(el => {
      const container = {};
      container.id = el.node.id || "";
      container.name = el.node.name || "";

      return container;
    });

    commit("set_genres", genres);
  },

  async fetchArtists({ commit }) {
    // console.log(state.pageLoaded);
    const clientApollo = this.app.apolloProvider.defaultClient;
    const response = await clientApollo.query({
      query: getArtists,
      variables: { first: 2000, after: "0" }
    });
    let artists = response.data.artists.edges.map(el => {
      const container = {};
      container.id = el.node.id || "";
      container.name = el.node.name || "";

      return container;
    });

    commit("set_artists", artists);
  }
  // async getArtistAlbums({ commit }, payload) {
  //   return new Promise((resolve, reject) => {
  //     this.$axios
  //       .post("/list_albums", payload)
  //       .then(resp => {
  //         commit("set_albums", resp.data.data.albums);
  //         resolve({ albums: resp.data.data.albums });
  //       })
  //       .catch(err => {
  //         reject(err);
  //       });
  //   });
  // }
};
