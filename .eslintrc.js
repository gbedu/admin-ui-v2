module.exports = {
  root: true,

  env: {
    browser: true,
    node: true
  },

  parser: 'vue-eslint-parser',

  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 2020
  },

  // https://github.com/feross/standard/blob/master/RULES.md#javascript-standard-style
  extends: 'plugin:vue/essential',

  // required to lint *.vue files
  plugins: [
    'vue'
  ],

  // add your custom rules here
  'rules': [
    'plugin:vue/essential',
    'eslint:recommended',
    '@vue/prettier'
  ],

  rules: {
    'arrow-parens': 0,
    'generator-star-spacing': 0,
    'vue/no-unused-components': 0,
    'vue/no-unused-vars': 0,
    'vue/return-in-computed-property': 0,
    'vue/return-in-computed-property': 0,
    'vue/no-duplicate-attributes': 0,
    'vue/no-use-v-if-with-v-for': 0,
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  }
}
